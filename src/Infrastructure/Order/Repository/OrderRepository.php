<?php


namespace Food\Infrastructure\Order\Repository;


use Food\Domain\Cook\Model\Cook;
use Food\Domain\Order\Model\Order;
use Food\Domain\Order\Repository\OrderRepositoryInterface;
use Food\Domain\Order\ValueObject\OrderId;

class OrderRepository implements OrderRepositoryInterface
{
    public function getOneByIdOrderId($orderId): Order
    {
        echo 'Order by UUID';
    }

    public function findOneByIdOrderId($orderId): ?Order
    {
        echo 'Order found';
    }

    public function findOneByStatus(string $status): ?array
    {
        echo 'list of orders';
    }

    public function save(Order $order): void
    {
        echo 'order saved';
    }

}