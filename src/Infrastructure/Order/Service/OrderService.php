<?php


namespace Food\Infrastructure\Order\Service;

use Food\Domain\Client\Model\Client;
use Food\Domain\Courier\Model\Courier;
use Food\Domain\Order\Model\Order;
use Food\Domain\Order\Service\OrderServiceInterface;
use Food\Domain\Order\ValueObject\OrderId;
use Ramsey\Uuid\Uuid;

class OrderService implements OrderServiceInterface
{
    public function createNew(Client $client): Order
    {
        //TODO: STATEMACHINE
        return new Order(new OrderId(Uuid::uuid4()->toString()), 'new', $client);
    }

    public function changeStatus(Order $order, string $status): Order
    {
        $order->changeStatus($status);
        return  $order;
    }

    public function assignCourier(Order $order, Courier $courier): Order
    {
        $order->assignCourier($courier);
        return  $order;
    }

    public function reassignCourier(Order $order, Courier $courier): Order
    {
        $order->reAssignCourier($courier);
        return  $order;
    }
}