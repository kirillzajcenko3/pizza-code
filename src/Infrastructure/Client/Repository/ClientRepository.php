<?php


namespace Food\Infrastructure\Client\Repository;


use Food\Domain\Client\Model\Client;
use Food\Domain\Client\Repository\ClientRepositoryInterface;
use Food\Domain\Client\ValueObject\ClientId;

class ClientRepository implements ClientRepositoryInterface
{
    public function getOneByIdClientId($userId): Client
    {
        // TODO: Implement getOneById) method.
    }

    public function findOneByIdClientId($userId): ?Client
    {
        // TODO: Implement findOneById) method.
    }

    public function findOneByName(string $username): ?Client
    {
        // TODO: Implement findOneByName() method.
    }

    public function save(Client $user): void
    {
        // TODO: Implement save() method.
    }

}