<?php


namespace Food\Infrastructure\Cook\Repository;


use Food\Domain\Cook\Model\Cook;
use Food\Domain\Cook\Repository\CookRepositoryInterface;
use Food\Domain\Cook\ValueObject\CookId;

class CookRepository implements CookRepositoryInterface
{
    public function getOneByIdCookId($cookId): Cook
    {
        // TODO: Implement getOneById) method.
    }

    public function findOneByIdCookId($cookId): ?Cook
    {
        // TODO: Implement findOneById) method.
    }

    public function findOneByName(string $username): ?Cook
    {
        // TODO: Implement findOneByName() method.
    }

    public function save(Cook $user): void
    {
        // TODO: Implement save() method.
    }

}