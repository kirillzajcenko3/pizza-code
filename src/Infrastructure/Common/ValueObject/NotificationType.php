<?php

declare(strict_types=1);

namespace Food\ValueObject;

class NotificationType
{
    private string $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getValue(): string
    {
        return $this->type;
    }
}
