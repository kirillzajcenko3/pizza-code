<?php


namespace Food\Infrastructure\Courier\Repository;


use Food\Domain\Courier\Model\Courier;
use Food\Domain\Courier\Repository\CourierRepositoryInterface;
use Food\Domain\Courier\ValueObject\CourierId;

class CourierRepository implements CourierRepositoryInterface
{
    public function getOneByIdCourierId($courier): Courier
    {
        // TODO: Implement getOneById) method.
    }

    public function findOneByIdCourierId($courier): ?Courier
    {
        // TODO: Implement findOneById) method.
    }

    public function findOneByName(string $name): ?Courier
    {
        // TODO: Implement findOneByName() method.
    }

    public function save(Courier $courier): void
    {
        // TODO: Implement save() method.
    }

}