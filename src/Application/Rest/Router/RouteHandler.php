<?php


namespace Food\Application\Rest\Router;

use Food\Application\Rest\Controller\ClientsController;
use Food\Application\Rest\Controller\CooksController;
use Food\Application\Rest\Controller\Couriers;
use Food\Application\Rest\Controller\DTO\Request;
use Food\Application\Rest\Controller\MainController;
use Food\Application\Rest\Controller\OrdersController;
use phputil\di\DI;

class RouteHandler
{
    public static function handle()
    {
        $controllerMap = [
            '' => DI::create(MainController::class),
            'clients' => DI::create(ClientsController::class),
            'cooks' => DI::create(CooksController::class),
            'couriers' => DI::create(Couriers::class),
            'orders' => DI::create(OrdersController::class),
        ];

        $requestUrl = explode('/', $_SERVER['REQUEST_URI']);

        $controller = $requestUrl[1];
        if (!key_exists($controller, $controllerMap)) {
            http_response_code(404);
            echo 404;
            return;
        }

        $actionName = $requestUrl[2] ?? null;
        $action = $actionName ? explode('?', $actionName)[0] : 'main';

        $controllerObject = $controllerMap[$controller];

        if (!method_exists($controllerObject, $action)) {
            http_response_code(404);
            echo 404;
            return;
        }

        $controllerObject->$action(Request::createFromPhpRequest());
    }
}