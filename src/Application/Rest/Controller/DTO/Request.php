<?php


namespace Food\Application\Rest\Controller\DTO;


class Request
{
    private array $paramBag = [];

    public static function createFromPhpRequest()
    {
        $obj = new self();
        foreach ($_REQUEST as $key => $value)
        {
            $obj->set($key, $value);
        }

        return $obj;
    }

    private function set(string $key, $value)
    {
        $this->paramBag[$key] = $value;
    }

    private function get(string $key)
    {
        return $this->paramBag[$key] ?? null;
    }
}