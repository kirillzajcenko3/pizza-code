<?php


namespace Food\Application\Rest\Controller;


use Food\Application\Rest\Controller\DTO\Request;
use Food\Domain\Order\Service\OrderServiceInterface;

class MainController extends AbstractController
{
    /**
     * @var OrderServiceInterface
     */
    private OrderServiceInterface $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    public function main(Request $request)
    {
        echo 'main';
    }
}