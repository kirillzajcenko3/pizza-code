<?php

namespace Food\Application\Rest\Config\DI;

use Food\Domain\Order\Service\OrderServiceInterface;
use Food\Infrastructure\Order\Service\OrderService;
use phputil\di\DI;

class DIServicesConfigurator
{
    public static function configureServices(): void
    {
        DI::config(DI::let( OrderServiceInterface::class)->create( OrderService::class ));
    }
}