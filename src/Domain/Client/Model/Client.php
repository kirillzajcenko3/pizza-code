<?php

namespace Food\Domain\Client\Model;

use Food\Domain\Common\ValueObject\AggregateRoot;
use Food\Domain\Client\ValueObject\ClientId;
use Food\Infrastructure\Common\ValueObject\Email;

class Client extends AggregateRoot
{
    /**
     * @var ClientId
     */
    protected $id;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var null|\DateTime
     */
    private $updatedAt;

    public function __construct(
        ClientId $clientId,
        string $name,
        Email $email
    ) {
        parent::__construct($clientId);

        $this->name = $name;
        $this->setEmail($email);
        $this->createdAt = new \DateTime();
    }

    public static function create(
        ClientId $clientId,
        string $name,
        Email $email
    ): Client {

        $client = new self($clientId, $name, $email);

        return $client;
    }
    

    private function setEmail(Email $email)
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
}
