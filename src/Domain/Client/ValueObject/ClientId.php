<?php

namespace Food\Domain\Client\ValueObject;

use Food\Domain\Common\ValueObject\AggregateRootId;

class ClientId extends AggregateRootId
{
    /** @var  string */
    protected $id;
}
