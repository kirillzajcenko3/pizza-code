<?php

namespace Food\Domain\Client\Exception;

use Food\Domain\Common\Exception\NotFoundException;

class ClientNotFoundException extends NotFoundException
{
    public function __construct()
    {
        parent::__construct("client.exception.not_found", 2004);
    }
}
