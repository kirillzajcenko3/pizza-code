<?php

namespace Food\Domain\Client\Repository;

use Food\Domain\Client\Exception\ClientNotFoundException;
use Food\Domain\Client\Model\Client;

interface ClientRepositoryInterface
{
    /**
     * @throws ClientNotFoundException
     */
    public function getOneByIdClientId($userId): Client;

    public function findOneByIdClientId($userId): ?Client;

    public function findOneByName(string $username): ?Client;

    public function save(Client $user): void;
}
