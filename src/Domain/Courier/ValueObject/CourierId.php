<?php

namespace Food\Domain\Courier\ValueObject;

use Food\Domain\Common\ValueObject\AggregateRootId;

class CourierId extends AggregateRootId
{
    /** @var  string */
    protected $id;
}
