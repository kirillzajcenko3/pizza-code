<?php

namespace Food\Domain\Courier\Exception;

use Food\Domain\Common\Exception\NotFoundException;

class CourierNotFoundException extends NotFoundException
{
    public function __construct()
    {
        parent::__construct("courier.exception.not_found", 2004);
    }
}
