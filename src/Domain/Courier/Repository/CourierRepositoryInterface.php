<?php

namespace Food\Domain\Courier\Repository;

use Food\Domain\Courier\Exception\CourierNotFoundException;
use Food\Domain\Courier\Model\Courier;

interface CourierRepositoryInterface
{
    /**
     * @throws CourierNotFoundException
     */
    public function getOneByIdCourierId($courier): Courier;

    public function findOneByIdCourierId($courier): ?Courier;

    public function findOneByName(string $name): ?Courier;

    public function save(Courier $courier): void;
}
