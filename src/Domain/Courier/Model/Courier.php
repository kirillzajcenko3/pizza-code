<?php

namespace Food\Domain\Courier\Model;

use Food\Domain\Common\ValueObject\AggregateRoot;
use Food\Domain\Courier\ValueObject\CourierId;

class Courier extends AggregateRoot
{
    /**
     * @var CourierId
     */
    protected $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var null|\DateTime
     */
    private $updatedAt;

    public function __construct(
        CourierId $courier,
        string $name
    ) {
        parent::__construct($courier);

        $this->name = $name;
        $this->createdAt = new \DateTime();
    }

    public static function create(
        CourierId $courierId,
        string $name
    ): Courier {

        $courier = new self($courierId, $name);

        return $courier;
    }
    
    public function getName(): string
    {
        return $this->name;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
}
