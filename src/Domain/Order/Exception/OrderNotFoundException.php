<?php

namespace Food\Domain\Order\Exception;

use Food\Domain\Common\Exception\NotFoundException;

class OrderNotFoundException extends NotFoundException
{
    public function __construct()
    {
        parent::__construct("order.exception.not_found", 2004);
    }
}
