<?php

namespace Food\Domain\Order\Repository;

use Food\Domain\Cook\Model\Cook;
use Food\Domain\Order\Exception\OrderNotFoundException;
use Food\Domain\Order\Model\Order;
use Food\Domain\Order\ValueObject\OrderId;

interface OrderRepositoryInterface
{
    /**
     * @throws OrderNotFoundException
     */
    public function getOneByIdOrderId($orderId): Order;

    public function findOneByIdOrderId($orderId): ?Order;

    public function findOneByStatus(string $status): ?array;

    public function save(Order $order): void;
}
