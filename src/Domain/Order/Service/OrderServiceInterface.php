<?php


namespace Food\Domain\Order\Service;


use Food\Domain\Client\Model\Client;
use Food\Domain\Courier\Model\Courier;
use Food\Domain\Order\Model\Order;

interface OrderServiceInterface
{
    public function createNew(Client $client): Order;
    public function changeStatus(Order $order, string $status): Order;
    public function assignCourier(Order $order, Courier $courier): Order;
    public function reassignCourier(Order $order, Courier $courier): Order;
}