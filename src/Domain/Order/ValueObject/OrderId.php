<?php

namespace Food\Domain\Order\ValueObject;

use Food\Domain\Common\ValueObject\AggregateRootId;

class OrderId extends AggregateRootId
{
    /** @var  string */
    protected $id;
}
