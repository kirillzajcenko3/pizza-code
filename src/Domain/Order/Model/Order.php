<?php

namespace Food\Domain\Order\Model;

use Food\Domain\Client\Model\Client;
use Food\Domain\Common\ValueObject\AggregateRoot;
use Food\Domain\Courier\Model\Courier;
use Food\Domain\Order\ValueObject\OrderId;

class Order extends AggregateRoot
{
    /**
     * @var OrderId
     */
    protected $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var null|\DateTime
     */
    private $updatedAt;

    /**
     * @var null|Courier
     */
    private $courier;

    /**
     * @var Client
     */
    private Client $client;

    public function __construct(
        OrderId $orderId,
        string $status,
        Client $client
    ) {
        parent::__construct($orderId);

        $this->status = $status;
        $this->createdAt = new \DateTime();
        $this->client = $client;
    }

    public static function create(
        OrderId $orderId,
        string $status,
        Client $client
    ): Order {

        $order = new self($orderId, $status, $client);

        return $order;
    }

    public function assignCourier(Courier $courier)
    {
        if($this->courier){
            throw new \DomainException('Unassign courier or user reassign');
        }
        $this->courier = $courier;
    }

    public function reAssignCourier(Courier $courier)
    {
        $this->courier = $courier;
    }

    public function getOrderStatus(): string
    {
        return $this->status;
    }

    public function changeStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
}
