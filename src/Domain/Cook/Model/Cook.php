<?php

namespace Food\Domain\Cook\Model;

use Food\Domain\Common\ValueObject\AggregateRoot;
use Food\Domain\Cook\ValueObject\CookId;

class Cook extends AggregateRoot
{
    /**
     * @var CookId
     */
    protected $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var null|\DateTime
     */
    private $updatedAt;

    public function __construct(
        CookId $cookId,
        string $name
    ) {
        parent::__construct($cookId);

        $this->name = $name;
        $this->createdAt = new \DateTime();
    }

    public static function create(
        CookId $cookId,
        string $name
    ): Cook {

        $cook = new self($cookId, $name);

        return $cook;
    }
    
    public function getName(): string
    {
        return $this->name;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }
}
