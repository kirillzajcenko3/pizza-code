<?php

namespace Food\Domain\Cook\ValueObject;

use Food\Domain\Common\ValueObject\AggregateRootId;

class CookId extends AggregateRootId
{
    /** @var  string */
    protected $id;
}
