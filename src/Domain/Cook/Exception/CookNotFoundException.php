<?php

namespace Food\Domain\Cook\Exception;

use Food\Domain\Common\Exception\NotFoundException;

class CookNotFoundException extends NotFoundException
{
    public function __construct()
    {
        parent::__construct("cook.exception.not_found", 2004);
    }
}
