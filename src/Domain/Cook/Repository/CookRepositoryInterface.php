<?php

namespace Food\Domain\Cook\Repository;

use Food\Domain\Cook\Exception\CookNotFoundException;
use Food\Domain\Cook\Model\Cook;

interface CookRepositoryInterface
{
    /**
     * @throws CookNotFoundException
     */
    public function getOneByIdCookId($cookId): Cook;

    public function findOneByIdCookId($cookId): ?Cook;

    public function findOneByName(string $username): ?Cook;

    public function save(Cook $user): void;
}
