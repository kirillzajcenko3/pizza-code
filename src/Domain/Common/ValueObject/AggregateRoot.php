<?php

namespace Food\Domain\Common\ValueObject;

abstract class AggregateRoot
{
    /**
     * @var AggregateRootId
     */
    protected $id;

    protected function __construct(AggregateRootId $aggregateRootId)
    {
        $this->id = $aggregateRootId;
    }

    public function getId(): AggregateRootId
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return (string) $this->id;
    }
}
