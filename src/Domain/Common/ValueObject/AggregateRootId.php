<?php

declare(strict_types=1);

namespace Food\Domain\Common\ValueObject;

/**
 * Class AggregateRootId
 *
 * Its the unique identifier and will be auto-generated if not value is set.
 *
 * @package Food\Domain\Common\ValueObject
 */
abstract class AggregateRootId
{
    /**
     * @var string
     */
    protected $id;

    public function __construct(string $id)
    {
       $this->id = $id;
    }

    public function __toString(): string
    {
        return (string) $this->id;
    }
}
