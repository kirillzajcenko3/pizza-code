<?php

use Food\Entity\Guest;
use Food\Entity\User;
use Food\Service\EmailDataExtractor;
use Food\Service\ExtractorRegistry;
use Food\Service\Notification\NotificationManager;
use Food\Service\SmsDataExtractor;
use Food\ValueObject\Email;
use Food\ValueObject\NotificationType;
use Food\ValueObject\Phone;
use Pizza\EmailNotificator;
use Pizza\NotificatorRegistry;
use Pizza\SmsNotificator;

require_once __DIR__.'./../vendor/autoload.php';

$emailNotificator = new EmailNotificator();
$smsNotificator = new SmsNotificator();

$notifRgistry = new NotificatorRegistry();
$notifRgistry->register($emailNotificator);
$notifRgistry->register($smsNotificator);

$smsExtractro = new SmsDataExtractor();
$emExtractro = new EmailDataExtractor();
$extrRegistry = new ExtractorRegistry();
$extrRegistry->register($smsExtractro);
$extrRegistry->register($emExtractro);

$notificationManager = new NotificationManager($notifRgistry, $extrRegistry);


//---------DOMAIN-------------//
$email = new Email();
$phone = new Phone();
$notifTypeE = new NotificationType('email');
$notifTypeS = new NotificationType('sms');


$user1 = new User(1,$email, $phone, $notifTypeE);
$user2 = new User(2,$email, $phone, $notifTypeS);
$guest= new Guest(1,$email, $notifTypeE);

$notificationManager->notify($user1);
$notificationManager->notify($user2);
$notificationManager->notify($guest);
